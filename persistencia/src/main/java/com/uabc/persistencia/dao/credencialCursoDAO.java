package com.uabc.persistencia.dao;

import com.uabc.entidad.Credencialcurso;
import com.uabc.persistencia.persistencia.AbstractDAO;
import java.util.List;

/**
 *
 * @author Axel Valenzuela - Complejo Acuatico 2018
 */
public class credencialCursoDAO extends AbstractDAO<Integer, Credencialcurso> {

    /**
     *
     * @param curso
     */
    public void addCredencialCurso(Credencialcurso curso) {
        this.save(curso);
    }

    /**
     *
     * @param curso
     */
    public void updateCredencialCurso(Credencialcurso curso) {
        this.update(curso);
    }

    /**
     *
     * @return
     */
    public List<Credencialcurso> findAllCredencialCurso() {
        return this.findAll();
    }

    /**
     *
     * @param id
     * @return
     */
    public Credencialcurso findByCredencialCursoId(int id) {
        return this.find(id);
    }

}
