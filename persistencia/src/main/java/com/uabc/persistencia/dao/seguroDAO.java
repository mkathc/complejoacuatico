package com.uabc.persistencia.dao;

import com.uabc.entidad.Seguro;
import com.uabc.persistencia.persistencia.AbstractDAO;
import java.util.List;

/**
 *
 * @author Axel Valenzuela - Complejo Acuatico 2018
 */
public class seguroDAO extends AbstractDAO<Integer, Seguro> {

    public void addSeguro(Seguro seguro) {
        this.save(seguro);
    }

    public void updateCurso(Seguro seguro) {
        this.update(seguro);
    }

    public List<Seguro> findAllSeguro() {
        return this.findAll();
    }

    public Seguro findBySeguroId(int id) {
        return this.find(id);
    }

}
