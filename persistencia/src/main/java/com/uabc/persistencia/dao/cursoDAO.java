package com.uabc.persistencia.dao;

import com.uabc.entidad.Curso;
import com.uabc.persistencia.persistencia.AbstractDAO;
import java.util.List;

/**
 *
 * @author Axel Valenzuela - Complejo Acuatico 2018
 */
public class cursoDAO extends AbstractDAO<Integer, Curso> {

    public void addCurso(Curso curso) {
        this.save(curso);
    }

    public void updateCurso(Curso curso) {
        this.update(curso);
    }

    public List<Curso> findAllCurso() {
        return this.findAll();
    }

    public Curso findByCursoId(int id) {
        return this.find(id);
    }

}
