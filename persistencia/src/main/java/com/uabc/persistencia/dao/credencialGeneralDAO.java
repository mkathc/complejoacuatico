package com.uabc.persistencia.dao;

import com.uabc.entidad.Credencialgeneral;
import com.uabc.persistencia.persistencia.AbstractDAO;
import java.util.List;
/**
 *
 * @author Axel Valenzuela - Complejo Acuatico 2018
 */
public class credencialGeneralDAO extends AbstractDAO<Integer, Credencialgeneral> {

    /**
     *
     * @param general
     */
    public void addCredencialGeneral(Credencialgeneral general) {
        this.save(general);
    }

    /**
     *
     * @param general
     */
    public void updateCredencialGeneral(Credencialgeneral general) {
        this.update(general);
    } 

    /**
     *
     * @return
     */
    public List<Credencialgeneral> findAllCredencialGeneral() {
        return this.findAll();
    }

    /**
     *
     * @param id
     * @return
     */
    public Credencialgeneral findByCredencialGeneralId(int id) {
        return this.find(id);
    } 
    
}