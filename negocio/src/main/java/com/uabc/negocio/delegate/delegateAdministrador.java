package com.uabc.negocio.delegate;

import com.uabc.entidad.Administrador; 
import com.uabc.persistencia.integracion.ServiceLocator;

import java.util.List;

/** 
 * @author Axel Valenzuela - Complejo Acuático 2018
 */
public class delegateAdministrador {

    /**
     *
     * @param administrador que se va a guardar
     */
    public void saveAdministrador(Administrador administrador) { 
            ServiceLocator.getInstanceAdminDAO().addAdmin(administrador);
//        }
    }

    /**
     * @param id de la Administrador que se va a buscar
     * @return Administrador encontrada
     */
    public Administrador findByID(int id) {
        return ServiceLocator.getInstanceAdminDAO().findByAdminId(id);
    }

    /**
     * @return Todos los Administrador encontrados
     */
    public List<Administrador> findAdministrador() {
        return ServiceLocator.getInstanceAdminDAO().findAllAdmin();
    }

    /**
     * @param administrador para actualizar
     */
    public void updateAdministrador(Administrador administrador) {
        ServiceLocator.getInstanceAdminDAO().updateAdmin(administrador);
    }

    /**
     * @param id
     */
    public void deleteAdministrador(int id) {
        ServiceLocator.getInstanceAdminDAO().deleteAdmin(id);
    }

}
