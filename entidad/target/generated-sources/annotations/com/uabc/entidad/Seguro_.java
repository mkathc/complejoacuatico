package com.uabc.entidad;

import com.uabc.entidad.Administrador;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-12-16T17:22:55")
@StaticMetamodel(Seguro.class)
public class Seguro_ { 

    public static volatile SingularAttribute<Seguro, String> apellidoPaterno;
    public static volatile SingularAttribute<Seguro, String> folioSeguro;
    public static volatile SingularAttribute<Seguro, Integer> numeroSeguro;
    public static volatile SingularAttribute<Seguro, Administrador> idAdministrador;
    public static volatile SingularAttribute<Seguro, Integer> idSeguro;
    public static volatile SingularAttribute<Seguro, Date> fechaNacimiento;
    public static volatile SingularAttribute<Seguro, String> correo;
    public static volatile SingularAttribute<Seguro, String> egresado;
    public static volatile SingularAttribute<Seguro, String> nombre;
    public static volatile SingularAttribute<Seguro, String> apellidoMaterno;

}