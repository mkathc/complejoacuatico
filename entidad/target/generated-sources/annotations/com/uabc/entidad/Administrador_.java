package com.uabc.entidad;

import com.uabc.entidad.Seguro;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-12-16T17:22:55")
@StaticMetamodel(Administrador.class)
public class Administrador_ { 

    public static volatile SingularAttribute<Administrador, String> apellidoPaterno;
    public static volatile SingularAttribute<Administrador, String> puesto;
    public static volatile SingularAttribute<Administrador, Integer> idAdministrador;
    public static volatile SingularAttribute<Administrador, String> nombreAdministrador;
    public static volatile SingularAttribute<Administrador, String> cuentaAdministrador;
    public static volatile ListAttribute<Administrador, Seguro> seguroList;
    public static volatile SingularAttribute<Administrador, String> contraseñaAdministador;
    public static volatile SingularAttribute<Administrador, String> apellidoMaterno;

}