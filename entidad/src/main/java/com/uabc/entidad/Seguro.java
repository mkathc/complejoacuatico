/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uabc.entidad;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author revy
 */
@Entity
@Table(name = "seguro")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Seguro.findAll", query = "SELECT s FROM Seguro s")
    , @NamedQuery(name = "Seguro.findByIdSeguro", query = "SELECT s FROM Seguro s WHERE s.idSeguro = :idSeguro")
    , @NamedQuery(name = "Seguro.findByNombre", query = "SELECT s FROM Seguro s WHERE s.nombre = :nombre")
    , @NamedQuery(name = "Seguro.findByApellidoPaterno", query = "SELECT s FROM Seguro s WHERE s.apellidoPaterno = :apellidoPaterno")
    , @NamedQuery(name = "Seguro.findByApellidoMaterno", query = "SELECT s FROM Seguro s WHERE s.apellidoMaterno = :apellidoMaterno")
    , @NamedQuery(name = "Seguro.findByNumeroSeguro", query = "SELECT s FROM Seguro s WHERE s.numeroSeguro = :numeroSeguro")
    , @NamedQuery(name = "Seguro.findByEgresado", query = "SELECT s FROM Seguro s WHERE s.egresado = :egresado")
    , @NamedQuery(name = "Seguro.findByFechaNacimiento", query = "SELECT s FROM Seguro s WHERE s.fechaNacimiento = :fechaNacimiento")
    , @NamedQuery(name = "Seguro.findByCorreo", query = "SELECT s FROM Seguro s WHERE s.correo = :correo")
    , @NamedQuery(name = "Seguro.findByFolioSeguro", query = "SELECT s FROM Seguro s WHERE s.folioSeguro = :folioSeguro")})
public class Seguro implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idSeguro")
    private Integer idSeguro;
    @Basic(optional = false)
    @Column(name = "nombre")
    private String nombre;
    @Basic(optional = false)
    @Column(name = "apellidoPaterno")
    private String apellidoPaterno;
    @Basic(optional = false)
    @Column(name = "apellidoMaterno")
    private String apellidoMaterno;
    @Basic(optional = false)
    @Column(name = "numeroSeguro")
    private int numeroSeguro;
    @Basic(optional = false)
    @Column(name = "egresado")
    private String egresado;
    @Basic(optional = false)
    @Column(name = "fechaNacimiento")
    @Temporal(TemporalType.DATE)
    private Date fechaNacimiento;
    @Basic(optional = false)
    @Column(name = "correo")
    private String correo;
    @Basic(optional = false)
    @Column(name = "folioSeguro")
    private String folioSeguro;
    @JoinColumn(name = "idAdministrador", referencedColumnName = "idAdministrador")
    @ManyToOne(optional = false)
    private Administrador idAdministrador;

    public Seguro() {
    }

    public Seguro(Integer idSeguro) {
        this.idSeguro = idSeguro;
    }

    public Seguro(Integer idSeguro, String nombre, String apellidoPaterno, String apellidoMaterno, int numeroSeguro, String egresado, Date fechaNacimiento, String correo, String folioSeguro) {
        this.idSeguro = idSeguro;
        this.nombre = nombre;
        this.apellidoPaterno = apellidoPaterno;
        this.apellidoMaterno = apellidoMaterno;
        this.numeroSeguro = numeroSeguro;
        this.egresado = egresado;
        this.fechaNacimiento = fechaNacimiento;
        this.correo = correo;
        this.folioSeguro = folioSeguro;
    }

    public Integer getIdSeguro() {
        return idSeguro;
    }

    public void setIdSeguro(Integer idSeguro) {
        this.idSeguro = idSeguro;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    public int getNumeroSeguro() {
        return numeroSeguro;
    }

    public void setNumeroSeguro(int numeroSeguro) {
        this.numeroSeguro = numeroSeguro;
    }

    public String getEgresado() {
        return egresado;
    }

    public void setEgresado(String egresado) {
        this.egresado = egresado;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getFolioSeguro() {
        return folioSeguro;
    }

    public void setFolioSeguro(String folioSeguro) {
        this.folioSeguro = folioSeguro;
    }

    public Administrador getIdAdministrador() {
        return idAdministrador;
    }

    public void setIdAdministrador(Administrador idAdministrador) {
        this.idAdministrador = idAdministrador;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idSeguro != null ? idSeguro.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Seguro)) {
            return false;
        }
        Seguro other = (Seguro) object;
        if ((this.idSeguro == null && other.idSeguro != null) || (this.idSeguro != null && !this.idSeguro.equals(other.idSeguro))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.uabc.entidad.Seguro[ idSeguro=" + idSeguro + " ]";
    }
    
}
