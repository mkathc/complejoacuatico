/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uabc.entidad;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author revy
 */
@Entity
@Table(name = "credencialgeneral")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Credencialgeneral.findAll", query = "SELECT c FROM Credencialgeneral c")
    , @NamedQuery(name = "Credencialgeneral.findByIdcredencialgeneral", query = "SELECT c FROM Credencialgeneral c WHERE c.idcredencialgeneral = :idcredencialgeneral")
    , @NamedQuery(name = "Credencialgeneral.findBySeguro", query = "SELECT c FROM Credencialgeneral c WHERE c.seguro = :seguro")
    , @NamedQuery(name = "Credencialgeneral.findByTarifa", query = "SELECT c FROM Credencialgeneral c WHERE c.tarifa = :tarifa")})
public class Credencialgeneral implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idcredencialgeneral")
    private Integer idcredencialgeneral;
    @Basic(optional = false)
    @Column(name = "seguro")
    private String seguro;
    @Basic(optional = false)
    @Column(name = "tarifa")
    private int tarifa;
    @JoinColumn(name = "idCredencialDetalle", referencedColumnName = "idCredencialDetalle")
    @ManyToOne(optional = false)
    private Credencialdetalle idCredencialDetalle;

    public Credencialgeneral() {
    }

    public Credencialgeneral(Integer idcredencialgeneral) {
        this.idcredencialgeneral = idcredencialgeneral;
    }

    public Credencialgeneral(Integer idcredencialgeneral, String seguro, int tarifa) {
        this.idcredencialgeneral = idcredencialgeneral;
        this.seguro = seguro;
        this.tarifa = tarifa;
    }

    public Integer getIdcredencialgeneral() {
        return idcredencialgeneral;
    }

    public void setIdcredencialgeneral(Integer idcredencialgeneral) {
        this.idcredencialgeneral = idcredencialgeneral;
    }

    public String getSeguro() {
        return seguro;
    }

    public void setSeguro(String seguro) {
        this.seguro = seguro;
    }

    public int getTarifa() {
        return tarifa;
    }

    public void setTarifa(int tarifa) {
        this.tarifa = tarifa;
    }

    public Credencialdetalle getIdCredencialDetalle() {
        return idCredencialDetalle;
    }

    public void setIdCredencialDetalle(Credencialdetalle idCredencialDetalle) {
        this.idCredencialDetalle = idCredencialDetalle;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idcredencialgeneral != null ? idcredencialgeneral.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Credencialgeneral)) {
            return false;
        }
        Credencialgeneral other = (Credencialgeneral) object;
        if ((this.idcredencialgeneral == null && other.idcredencialgeneral != null) || (this.idcredencialgeneral != null && !this.idcredencialgeneral.equals(other.idcredencialgeneral))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.uabc.entidad.Credencialgeneral[ idcredencialgeneral=" + idcredencialgeneral + " ]";
    }
    
}
